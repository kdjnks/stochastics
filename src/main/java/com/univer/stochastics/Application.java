package com.univer.stochastics;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.*;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application extends JFrame {

    private JFrame jFrame;
    private JButton jButton;
    private JTextField jTextField;
    private volatile JProgressBar jProgressBar;

    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    private AtomicIntegerArray result = new AtomicIntegerArray(100);
    private AtomicInteger iteration = new AtomicInteger(0);

    private AtomicInteger maxIterations;
    private AtomicBoolean paused = new AtomicBoolean(false);

    private Random random = new Random();

    public static void main(String[] args) {
        new Application();
    }

    public Application() {
        run();
    }

    private void run() {
        this.jFrame = new JFrame("Random test");

        // create a panel
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        // create a progressbar
       this.jProgressBar = new JProgressBar();
        // set initial value
        this.jProgressBar.setValue(0);

        this.jProgressBar.setStringPainted(true);

        this.jTextField = new JTextField();
        this.jTextField.setToolTipText("Iterations");
        this.jTextField.setText("4000000");
        ((AbstractDocument)jTextField.getDocument()).setDocumentFilter(new DocumentFilter(){
            Pattern regEx = Pattern.compile("\\d*");

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
                Matcher matcher = regEx.matcher(text);
                if(!matcher.matches()){
                    return;
                }

                super.replace(fb, offset, length, text, attrs);
            }
        });

        this.jButton = new JButton("Start");
        this.jButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        jButton.addActionListener((event) -> {
            switch (jButton.getText()) {
                case "Start":
                    if(jTextField.getText().isEmpty()) return;

                    maxIterations = new AtomicInteger(Integer.parseInt(jTextField.getText()));
                    paused.set(false);

                    jButton.setText("Stop");

                    executorService.submit(() -> {
                        while(iteration.intValue() != maxIterations.intValue() && !paused.get()) {
                            result.incrementAndGet(random.nextInt(100));
                            iteration.incrementAndGet();
                            jProgressBar.setValue( (int) ((iteration.doubleValue() / maxIterations.doubleValue()) * 100));
                        }

                        if(iteration.intValue() == maxIterations.get()) {
                            jButton.setText("Start");
                        }

                        System.out.println("RESULTS:");
                        for (int i = 0; i < result.length(); i++) {
                            System.out.println(result.get(i));
                        }

                        printChart(result);
                    });
                    break;
                case "Stop":
                    jButton.setText("Start");
                    paused.set(true);
                    break;
            }
        });

        // add progressbar
        panel.add(this.jTextField);
        panel.add(this.jButton);
        panel.add(this.jProgressBar);


        // add panel
        this.jFrame.add(panel);

        // set the size of the frame
        this.jFrame.setVisible(true);
        this.jFrame.setResizable(false);
        this.jFrame.setPreferredSize(new Dimension(640, 120));
        this.jFrame.pack();
        this.jFrame.setLocationRelativeTo(null);
    }

    private void printChart(AtomicIntegerArray result) {
        XYSeriesCollection collectionXY = new XYSeriesCollection();
        for (int i = 0; i < result.length(); i++) {
            XYSeries point = new XYSeries(String.valueOf(i));
            System.out.println(result.get(i) / iteration.doubleValue());
            point.add(i, result.get(i) / iteration.doubleValue());
            collectionXY.addSeries(point);
        }

        new Chart("Results", collectionXY);
    }

    private class Chart extends ApplicationFrame {

        /**
         * Constructs a new application frame.
         *
         * @param title the frame title.
         */
        private Chart(String title, XYSeriesCollection collection) {
            super(title);
            JFreeChart xylineChart = ChartFactory.createXYLineChart(
                    title,
                    "X" ,
                    "Y" ,
                    collection,
                    PlotOrientation.VERTICAL ,
                    true , true , false);

            ChartPanel chartPanel = new ChartPanel( xylineChart );
            chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
            final XYPlot plot = xylineChart.getXYPlot( );
            ValueAxis domainAxis = plot.getDomainAxis();
            ValueAxis rangeAxis = plot.getRangeAxis();

            domainAxis.setRange(0.0, 100.0);
            rangeAxis.setRange(0.0, 1.0);

            XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
            renderer.setSeriesPaint( 0 , Color.RED );
            renderer.setSeriesPaint( 1 , Color.GREEN );
            renderer.setSeriesPaint( 2 , Color.YELLOW );
            renderer.setSeriesStroke( 0 , new BasicStroke( 4.0f ) );
            renderer.setSeriesStroke( 1 , new BasicStroke( 3.0f ) );
            renderer.setSeriesStroke( 2 , new BasicStroke( 2.0f ) );
            plot.setRenderer( renderer );
            setContentPane( chartPanel );
            this.pack();
            this.setLocationRelativeTo(null);
            this.setVisible( true );
        }
    }
}
