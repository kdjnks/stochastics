package com.univer.stochastics.models;

import java.util.Random;
import java.util.concurrent.Callable;

public class RandomCallable implements Callable<int[]> {

    private int[] result = new int[99];
    private int maxIteration;

    private volatile boolean isPaused = false;
    private volatile int iteration = 0;

    private Random random = new Random();

    public RandomCallable(int maxIteration) {
        this.maxIteration = maxIteration;
    }

    @Override
    public int[] call() {
        while (iteration != maxIteration) {
            if (!isPaused) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Generated.");
                result[random.nextInt(99)] += 1;
                iteration++;
            }
        }

        return result;
    }

    public synchronized int[] pause() {
        isPaused = true;
        return result;
    }

    public synchronized void resume() {
        isPaused = false;
    }

    public synchronized int getIteration() {
        return iteration;
    }

    public boolean isPaused() {
        return isPaused;
    }
}
